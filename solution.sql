-- all artists with a 
SELECT * FROM artists WHERE name LIKE "%d%";

-- all albums that start with b
SELECT * FROM albums WHERE album_title LIKE "b%";

-- join artists and albums tables
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id
WHERE name LIKE "a%";

-- sort the albums Z-A
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Sort artists A-Z
SELECT * FROM artists ORDER BY name ASC LIMIT 5;